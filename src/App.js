import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Dashboard from './Pages/Dashboard/Dashboard';
function App() {
  return (
    <BrowserRouter >
      <Switch>
        <Route path="/" exact>
          <Dashboard/>
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
