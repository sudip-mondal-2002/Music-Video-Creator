import React from 'react';
import MyNavbar from '../../Components/Navbar/Navbar';
import VideoModal from '../../Components/VideoModal/VideoModal';
import BgMusicModal from '../../Components/BgMusicModal/BgMusicModal';
import { Button } from 'react-bootstrap';
import "./Dashboard.css";
const Dashboard = () => {
    const [camera, setCamera] = React.useState(false);
    const [chooseBg, setChooseBg] = React.useState(false);
    const [url, setUrl] = React.useState("")
    return (
        <div>
            <MyNavbar />
            <div className="btnHolder">
                <Button variant="outline-success" onClick={() => { setChooseBg(true) }}>Choose background</Button>
                <Button variant="outline-danger" onClick={() => { setCamera(true) }}>start recording</Button>
            </div>
            {camera && <VideoModal setShow={setCamera} bgUrl={url}/>}
            {chooseBg && <BgMusicModal setShow={setChooseBg} url={url} setUrl={setUrl}/>}
        </div>
    );
}

export default Dashboard;
