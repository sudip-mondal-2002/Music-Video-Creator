import React from 'react';
import "./VideoModal.css";
import { Button } from 'react-bootstrap';
import "bootstrap/dist/css/bootstrap.css";
import { RecordCircle, Download, StopCircle } from 'react-bootstrap-icons';
import {VideoStreamMerger} from 'video-stream-merger'
import ReactPlayer from 'react-player/youtube'

const VideoModal = ({ setShow, bgUrl }) => {
    const [recording, setRecording] = React.useState(false);
    const [recordingSrc, setRecordingSrc] = React.useState(null);
    const [recorder, setRecorder] = React.useState(null);
    const [mergedStream, setMergedStream] = React.useState(null);
    const [playState, setPlayState] = React.useState(false);
    const myVideo = React.useRef();
    const bgMusic = React.useRef();
    React.useEffect(()=>{
        if (playState){
            recorder.start();
            setRecording(true);
            setRecordingSrc(null);
        }
    },[playState,recorder])
    React.useEffect(() => {
        if (mergedStream) {
            setRecorder(new MediaRecorder(mergedStream, { mimeType: "video/webm; codecs=vp9" }))
        }
    }, [mergedStream]);

    React.useEffect(() => {
        navigator.mediaDevices.getUserMedia({ video: true, audio: true })
            .then((currentStream) => {
                myVideo.current.srcObject = currentStream;
                myVideo.current.play();
                navigator.mediaDevices.getDisplayMedia({ video: true, audio: true })
                    .then((displayStream) => {
                        var merger = new VideoStreamMerger();
                        merger.addStream(displayStream, {
                            width: 0,
                            height: 0,
                            mute: false
                        })
                        merger.addStream(currentStream, {
                            x: 0,
                            y: 0,
                            width: merger.width,
                            height: merger.height,
                            mute: false
                        })
                        merger.start();
                        setMergedStream(merger.result);
                    })
            });
    }, [])

    const startRecording = () => {
        let data = [];
        recorder.ondataavailable = event => data.push(event.data);
        setPlayState(true);
    }

    const stopRecording = () => {
        recorder.stop();
        setPlayState(false);
        bgMusic.current.seekTo(0,'seconds');
        recorder.ondataavailable = (event) => {
            setRecordingSrc(URL.createObjectURL(new Blob([event.data], { type: 'video/mp4' })));
            setRecording(false);
        }
    }

    const crossHandler = () => {
        setRecording(false);
        setRecordingSrc(null);
        setShow(false);
    }

    return (
        <div className="videoModalContainer">
            <div className="videoModal">
                <div className="cross" onClick={crossHandler}>+</div>
                <video playsInline muted ref={myVideo} />
                <br />
                <div className="btnHolder">
                    <div className="d-hidden">
                        <ReactPlayer url={bgUrl} ref={bgMusic} playing={playState} height="0px" width="0px"></ReactPlayer>
                    </div>
                    {recording ?
                        <Button variant="outline-danger" className="actionButton" onClick={stopRecording} >
                            <StopCircle />  Stop
                        </Button> :
                        <Button variant="outline-danger" className="actionButton" onClick={startRecording} >
                            <RecordCircle />  Record
                        </Button>
                    }
                    {recordingSrc && <Button download="download.mp4" variant="outline-info" className="actionButton" href={recordingSrc}><Download />  Save</Button>}
                </div>
            </div>
        </div>
    );
}

export default VideoModal;
