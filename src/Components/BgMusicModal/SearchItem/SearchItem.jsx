import React from 'react';
import "./SearchItem.css";
import "bootstrap/dist/css/bootstrap.css"
import { Button } from 'react-bootstrap';
const SearchItem = ({ item, setUrl }) => {
    return (
        <div className="searchItem">
            <div className="row">
                <div className="col-2">
                    <img src={item.thumbnails.default.url} alt="meaw" />
                </div>
                <div className="col-9">
                    <div className="itemTitle">
                        <span>{item.title}</span>
                    </div>
                    <div className="itemDesc">
                        <span>{item.description}</span>
                    </div>
                    <div className="itemDesc">
                        <span>{item.channelTitle}</span>
                    </div>
                </div>
                <div className="col-1">
                    <Button onClick = {()=>{setUrl(item.link)}}>Select</Button>
                </div>
            </div>
        </div>
    );
}

export default SearchItem;