import React from 'react';
import "./BgMusicModal.css";
import ReactPlayer from 'react-player/youtube';
import ytSearch from 'youtube-search'
import { Form } from 'react-bootstrap';
import SearchItem from './SearchItem/SearchItem';
import "bootstrap/dist/css/bootstrap.css"
var opts = {
    maxResults: 20,
    key: 'AIzaSyCGgT7caa5DsyiPjHu1arbdMPkVp2hUn58'
}
const BgMusic = ({ setShow, setUrl, url }) => {
    const [search, setSearch] = React.useState("");
    const [searchResults, setSearchResults] = React.useState([]);
    React.useEffect(() => {
        ytSearch(search, opts).then(results => {
            setSearchResults(results.results);
        })
    }, [search]);

    const crossHandler = () => {
        setShow(false);
    }
    return (
        <div className="searchModalContainer">
            <div className="searchModal">
                <div className="cross" onClick={crossHandler}>+</div>
                <div className="d-flex justify-content-center">
                    <ReactPlayer url={url} style={{ border: "solid" }} />
                </div>
                <Form.Control
                    type="search"
                    placeholder="Search on Youtube"
                    value={search}
                    onChange={e => setSearch(e.target.value)}
                />
                <div className="scrollView my-3">
                    {
                        searchResults.map(item => {
                            return <SearchItem key={item.id} item={item} setUrl={setUrl} />
                        })
                    }
                </div>
            </div>
        </div>
    );
}

export default BgMusic;
