import React from 'react';
import {useHistory} from 'react-router-dom';
import {Navbar, Container,Nav, Button} from 'react-bootstrap'
import "bootstrap/dist/css/bootstrap.css";
const MyNavbar = () => {
    const history = useHistory();
    const goTo = (path) => {
        history.push(path);
    }
    return (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Container>
                <Navbar.Brand href="/">Music creator</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                    </Nav>
                    <Nav>
                        <Button variant="dark" onClick={()=>{goTo("/dashboard")}} >Dashboard</Button>
                        <Button variant="dark" onClick={()=>{goTo("/login")}}>Login | Sign up</Button>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default MyNavbar;
