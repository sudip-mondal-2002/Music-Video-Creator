# Music-Video-Creator
You can create music videos here, with background music from any youtube video. See the following developer instructions if you are a developer and if you want to use the website click [here](https://sudip-mondal-2002.github.io/Music-Video-Creator)
## Developer Istructions
You need to have the following things installed
* Node 12 LTS
* npm 6
After you install them you can follow the rest of the instructions

### Clone the repository

```cmd
> git clone https://github.com/sudip-mondal-2002/fake-news-detector.git
> cd fake-news-detector
```

### Install the dependencies

```cmd
> yarn install
```

<p align="center"> OR </p>

```cmd
> npm install
```

### Start the server

```cmd
> yarn run start
```
<p align="center"> OR </p>

```cmd
> npm start
```

Now go to your browser and browse to (http://localhost:3000)
